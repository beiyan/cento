package com.beiyan.cento.config;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.alibaba.druid.pool.DruidDataSource;
import com.mysql.jdbc.Driver;

@Configuration
public class DataSource {

    @Value("${beiyan.database.username}")
    private String username;
    @Value("${beiyan.database.password}")
    private String password;
    @Value("${beiyan.database.url}")
    private String url;

    @Bean
    public DruidDataSource getDataSource() throws SQLException {
        DruidDataSource ds = new DruidDataSource();
        ds.setUsername(username);
        ds.setPassword(password);
        ds.setUrl(url);
        ds.setDriver(new Driver());
        return ds;
    }

}
