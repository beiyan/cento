package com.beiyan.cento.config;

import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
//import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * @author： BeiYan
 * @Date： 2016-10-17 15:05
 * @Description: Spring session 配置
 */

//@Configuration
//@EnableRedisHttpSession
public class SpringSessionConfig {

    /**
     * redis 主机名
     */
    @Value("${beiyan.redis.hostname}")
    private String redishostname;
    /**
     * redis password
     */
    @Value("${beiyan.redis.password}")
    private String redisPassword;
    /**
     * redis port
     */
    @Value("${beiyan.redis.port}")
    private int redisPort;

   /* @Bean
    public JedisConnectionFactory connectionFactory() {
        JedisConnectionFactory factory = new JedisConnectionFactory();
        factory.setHostName(redishostname);
        factory.setPassword(redisPassword);
        factory.setPort(redisPort);
        return factory;
    }*/

}

