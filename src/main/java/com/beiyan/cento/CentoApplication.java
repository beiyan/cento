package com.beiyan.cento;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CentoApplication {
	public static void main(String[] args) {
		SpringApplication.run(CentoApplication.class, args);
	}
}
