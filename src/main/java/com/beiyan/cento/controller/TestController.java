package com.beiyan.cento.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/test")
public class TestController {

	@RequestMapping(value = "/hello", method = RequestMethod.GET)
	public @ResponseBody String hello() {
		return "Hello";
	}

	@RequestMapping(value = "/hi", method = RequestMethod.GET)
	public @ResponseBody String hi() {
		return "hi";
	}

}
