package com.beiyan.cento.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {

	@RequestMapping(value = "/")
	public String index() {
		return "index";
	}

	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public String test(Model model) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("name", "小明");
		map.put("address", "shanghai");
		map.put("phone", "121221122112");
		Map<String, Object> map2 = new HashMap<String, Object>();
		map2.put("name", "小黑");
		map2.put("address", "henan");
		map2.put("phone", "1881212");
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		list.add(map);
		list.add(map2);

		model.addAttribute("result", list);
		return "test";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String loginPage(@RequestParam(value = "error", required = false) String error) {
		return "login";
	}

}
