package com.beiyan.cento.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @author： BeiYan
 * @Date： 2016-10-12 21:47
 * @Description:
 */

public interface IUserService extends UserDetailsService {
    @Override
    UserDetails loadUserByUsername(String username);
}
