package com.beiyan.cento.service;

import com.beiyan.cento.respository.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

/**
 * @author： BeiYan
 * @Date： 2016-10-12 14:00
 * @Description: UserService
 */
@Service
public class UserService implements IUserService {

    @Autowired
    private UserRepository userRepository;

    public UserDetails loadUserByUsername(String username) {
        String phoneRegex = "^1(3|4|5|7|8)\\\\d{9}$";
        if (Pattern.compile(phoneRegex).matcher(username).matches()) {
            return userRepository.findByPhone(username);
        } else {
            return userRepository.findByUsername(username);
        }
    }
}

