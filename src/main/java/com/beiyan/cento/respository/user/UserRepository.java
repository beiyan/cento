package com.beiyan.cento.respository.user;

import com.beiyan.cento.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author： BeiYan
 * @Date： 2016-10-12 17:00
 * @Description:
 */

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);

    User findByPhone(String phone);

    /**
     * @Modifying 表明执行的操作是修改或者删除
     * hsql 中修改或者删除必须添加事物处理
     */
    @Transactional
    @Modifying
    @Query("delete from User where username =:username")
    void deleteByUsername(@Param("username") String username);
}

