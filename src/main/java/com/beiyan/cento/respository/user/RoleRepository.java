package com.beiyan.cento.respository.user;

import com.beiyan.cento.model.user.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author： BeiYan
 * @Date： 2016-10-12 16:49
 * @Description: 角色Repository
 */

public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role findByRoleName(String roleName);

    @Transactional
    @Modifying
    @Query("delete from Role where roleName =:roleName")
    void deleteByRoleName(@Param("roleName") String roleName);

}

