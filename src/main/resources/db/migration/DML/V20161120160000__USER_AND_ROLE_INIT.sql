--
--init role and user
--

INSERT INTO `role` (`id`, `role_name`, `description`) VALUES ('1', 'ROLE_USER', '普通用户');
INSERT INTO `role` (`id`, `role_name`, `description`) VALUES ('2', 'ROLE_ADMIN', '管理员角色');

INSERT INTO `user` (`username`, `password`, `phone`) VALUES ('user', '123456', '18817953320');
INSERT INTO `user` (`username`, `password`, `phone`) VALUES ('admin', '123456', '18817953321');
