--
--create  table user
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(50) NOT NULL COMMENT '用户登录名',
  `password` VARCHAR(100) NOT NULL COMMENT '密码',
  `phone` VARCHAR(11) NOT NULL COMMENT '手机号',
  `account_non_expired` TINYINT(1) NOT NULL DEFAULT 1 COMMENT '账号没有过期',
  `account_non_locked` TINYINT(1) NOT NULL DEFAULT 1 COMMENT '账号没有被锁定',
  `credentials_non_expired` TINYINT(1) NOT NULL DEFAULT 1 COMMENT '账号资质没有过期',
  `enabled` TINYINT(1) NOT NULL DEFAULT 1 COMMENT '账号可用',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC),
  UNIQUE INDEX `phone_UNIQUE` (`phone` ASC))
ENGINE = InnoDB;
