package com.beiyan.cento.aspectj;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class SingAspectj {

	@Before("execution(** com.beiyan.cento.aspectj.Sing.sing(..))")
	public void singBefore(JoinPoint joinPoint) {
		Object[] params = joinPoint.getArgs();
		System.out.println("开始打印参数.....");
		for (Object o : params) {
			System.out.println(o.toString());
		}
		System.out.println("注意了，歌星要唱歌了");
	}

	@After("execution(** com.beiyan.cento.aspectj.Sing.sing(..))")
	public void singAfter() {
		System.out.println("歌唱家唱歌结束");
	}

}
