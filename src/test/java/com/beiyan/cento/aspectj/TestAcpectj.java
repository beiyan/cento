package com.beiyan.cento.aspectj;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestAcpectj {

	@Autowired
	private Sing sing;
	
	@Test
	public void testSing(){
		sing.sing("刀郎", "冲动的惩罚");
	}
}
