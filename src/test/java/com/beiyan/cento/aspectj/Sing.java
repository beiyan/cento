package com.beiyan.cento.aspectj;

import org.springframework.stereotype.Component;

@Component
public class Sing {

	public void sing(String singer, String song) {
		System.out.println("The singer  " + singer + " is singing the song of " + song);
	}

}
