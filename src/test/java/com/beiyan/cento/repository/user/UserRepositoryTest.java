package com.beiyan.cento.repository.user;

import com.beiyan.cento.model.user.Role;
import com.beiyan.cento.model.user.User;
import com.beiyan.cento.respository.user.RoleRepository;
import com.beiyan.cento.respository.user.UserRepository;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author： BeiYan
 * @Date： 2016-10-12 16:57
 * @Description: User test
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
//@FixMethodOrder 指定Test的执行顺序
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;

    @Test
    public void test1UserAdd() {
        Role roleAdmin = roleRepository.findByRoleName("ADMIN");
        if (roleAdmin == null) {
            Role role1 = new Role();
            role1.setRoleName("ADMIN");
            role1.setDescription("this is a admin role");
            roleAdmin = roleRepository.save(role1);
        }
        Role roleUser = roleRepository.findByRoleName("USER");
        if (roleUser == null) {
            Role role2 = new Role();
            role2.setRoleName("USER");
            role2.setDescription("this is a user role");
            roleUser = roleRepository.save(role2);
        }
        List<Role> roles = new ArrayList<>();
        roles.add(roleAdmin);
        roles.add(roleUser);
        User user = new User();
        user.setUsername("beiyan");
        user.setPassword("password");
        user.setPhone("123456");
        user.setRoles(roles);
        //save the user
        userRepository.save(user);
    }

    @Test
    public void test2UserQuery() {
        User user = userRepository.findByUsername("beiyan");
        System.out.println(user);
        List<Role> roles = user.getRoles();
        for (Role r : roles) {
            System.out.println(r);
        }
    }


    @Test
    public void test3Authorities() {
        List<SimpleGrantedAuthority> authorities = (List<SimpleGrantedAuthority>) userRepository.findByUsername("beiyan").getAuthorities();
        Assert.assertNotNull(authorities);
        authorities.stream().forEach(e->{
            System.out.println(e.toString());
        });
    }

    @Test
    public void test4UserDelete() {
        userRepository.deleteByUsername("beiyan");
        roleRepository.deleteByRoleName("ADMIN");
        roleRepository.deleteByRoleName("USER");
    }

}

